let history = document.getElementById("history-value");
let result = document.getElementById("output-value");
let numbers = document.querySelectorAll("button");

for (item of numbers) {
  item.addEventListener("click", (e) => {
    let screenValue = "";
    buttonText = e.target.innerText;
    result.innerHTML += buttonText;
    if (buttonText == "C") {
      screenValue = "";
      result.value = screenValue;
      result.innerHTML = result.value;
    } else if (buttonText == "CE") {
      let str = result.innerHTML;
      result.innerHTML = str.substring(0, str.length - 3);
    } else if (buttonText == "=") {
      let str = result.innerHTML;
      result.innerHTML = str.substring(0, str.length - 1);
      let res = eval(result.innerHTML);
      console.log(res);
      result.innerHTML = res;
      history.innerHTML = res;
    }
  });
}
